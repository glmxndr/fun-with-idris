module Data.Tree23.Merge

import Data.Tree23.Types
import Data.Tree23.Lemmas

%default total

%hint
export
branch4 :  {ctx: TCtx} -> {a,b,c,d,e,f,g,h: ctx.key} 
        -> Tree23 ctx a b depth -> (0 bc: ctx.rel b c)
        -> Tree23 ctx c d depth -> (0 de: ctx.rel d e)
        -> Tree23 ctx e f depth -> (0 fg: ctx.rel f g)
        -> Tree23 ctx g h depth
        -> Tree23 ctx a h (S $ S depth)
branch4 ab b_lt_c cd d_lt_e ef f_lt_g gh =
  Branch2 
    (Branch2 ab cd {r0=b_lt_c}) 
    (Branch2 ef gh {r0=f_lt_g})
    {r0=d_lt_e}

%hint
export
branch5 : {ctx: TCtx} -> {a,b,c,d,e,f,g,h,i,j: ctx.key} 
        -> Tree23 ctx a b depth -> (0 bc: ctx.rel b c)
        -> Tree23 ctx c d depth -> (0 de: ctx.rel d e)
        -> Tree23 ctx e f depth -> (0 fg: ctx.rel f g)
        -> Tree23 ctx g h depth -> (0 hi: ctx.rel h i)
        -> Tree23 ctx i j depth
        -> Tree23 ctx a j (S $ S depth)
branch5 ab b_lt_c cd d_lt_e ef f_lt_g gh h_lt_i ij  =
  Branch2 
    (Branch2 ab cd {r0=b_lt_c}) 
    (Branch3 ef gh ij {r1=f_lt_g} {r2=h_lt_i})
    {r0=d_lt_e}


%hint
export
branch6 : {ctx: TCtx} -> {a,b,c,d,e,f,g,h,i,j,k,l: ctx.key} 
        -> Tree23 ctx a b depth -> (0 bc: ctx.rel b c)
        -> Tree23 ctx c d depth -> (0 de: ctx.rel d e)
        -> Tree23 ctx e f depth -> (0 fg: ctx.rel f g)
        -> Tree23 ctx g h depth -> (0 hi: ctx.rel h i)
        -> Tree23 ctx i j depth -> (0 jk: ctx.rel j k)
        -> Tree23 ctx k l depth
        -> Tree23 ctx a l (S $ S depth)
branch6 ab b_lt_c cd d_lt_e ef f_lt_g gh h_lt_i ij j_lt_k kl  =
  Branch3
    (Branch2 ab cd {r0=b_lt_c}) 
    (Branch2 ef gh {r0=f_lt_g})
    (Branch2 ij kl {r0=j_lt_k})
    {r1=d_lt_e}
    {r2=h_lt_i}


%hint
export
branch7 : {ctx: TCtx} -> {a,b,c,d,e,f,g,h,i,j,k,l,m,n: ctx.key} 
        -> Tree23 ctx a b depth -> (0 bc: ctx.rel b c)
        -> Tree23 ctx c d depth -> (0 de: ctx.rel d e)
        -> Tree23 ctx e f depth -> (0 fg: ctx.rel f g)
        -> Tree23 ctx g h depth -> (0 hi: ctx.rel h i)
        -> Tree23 ctx i j depth -> (0 jk: ctx.rel j k)
        -> Tree23 ctx k l depth -> (0 lm: ctx.rel l m)
        -> Tree23 ctx m n depth
        -> Tree23 ctx a n (S $ S depth)
branch7 ab b_lt_c cd d_lt_e ef f_lt_g gh h_lt_i ij j_lt_k kl l_lt_m mn  =
  Branch3
    (Branch3 ab cd ef {r1=b_lt_c} {r2=d_lt_e}) 
    (Branch2 gh ij {r0=h_lt_i})
    (Branch2 kl mn {r0=l_lt_m})
    {r1=f_lt_g}
    {r2=j_lt_k}

export
merge1 :  {ctx: TCtx} -> {a,b,c,d,e,f: ctx.key} 
       -> Tree23 ctx a b depth -> (0 bc: ctx.rel b c)
       -> Tree23 ctx c d (S depth) -> (0 de: ctx.rel d e)
       -> Tree23 ctx e f (S depth)
       -> Tree23 ctx a f (S (S depth))
merge1 ab b_lt_c (Branch2 cdl cdr {r0=cdlt}) d_lt_e (Branch2 efl efr {r0=eflt}) = 
  branch5 ab b_lt_c cdl cdlt cdr d_lt_e efl eflt efr 
merge1 ab b_lt_c (Branch2 cdl cdr {r0=cdlt}) d_lt_e (Branch3 efl efm efr {r1=eflt1} {r2=eflt2}) =
  branch6 ab b_lt_c cdl cdlt cdr d_lt_e efl eflt1 efm eflt2 efr
merge1 ab b_lt_c (Branch3 cdl cdm cdr {r1=cdlt1} {r2=cdlt2}) d_lt_e (Branch2 efl efr {r0=eflt}) =
  branch6 ab b_lt_c cdl cdlt1 cdm cdlt2 cdr d_lt_e efl eflt efr
merge1 ab b_lt_c (Branch3 cdl cdm cdr {r1=cdlt1} {r2=cdlt2}) d_lt_e (Branch3 efl efm efr {r1=eflt1} {r2=eflt2}) =
  branch7 ab b_lt_c cdl cdlt1 cdm cdlt2 cdr d_lt_e efl eflt1 efm eflt2 efr


export
merge2 :  {ctx: TCtx} -> {a,b,c,d,e,f: ctx.key}
       -> Tree23 ctx a b (S depth) -> (0 bc: ctx.rel b c)
       -> Tree23 ctx c d depth -> (0 de: ctx.rel d e)
       -> Tree23 ctx e f (S depth)
       -> Tree23 ctx a f (S (S depth)) 
merge2 (Branch2 abl abr {r0=ablt}) b_lt_c cd d_lt_e (Branch2 efl efr {r0=eflt1}) =
  branch5 abl ablt abr b_lt_c cd d_lt_e efl eflt1 efr
merge2 (Branch2 abl abr {r0=ablt}) b_lt_c cd d_lt_e (Branch3 efl efm efr {r1=eflt1} {r2=eflt2}) =
  branch6 abl ablt abr b_lt_c cd d_lt_e efl eflt1 efm eflt2 efr
merge2 (Branch3 abl abm abr {r1=ablt1} {r2=ablt2}) b_lt_c cd d_lt_e (Branch2 efl efr {r0=eflt1}) =
  branch6 abl ablt1 abm ablt2 abr b_lt_c cd d_lt_e efl eflt1 efr
merge2 (Branch3 abl abm abr {r1=ablt1} {r2=ablt2}) b_lt_c cd d_lt_e (Branch3 efl efm efr {r1=eflt1} {r2=eflt2}) =
  branch7 abl ablt1 abm ablt2 abr b_lt_c cd d_lt_e efl eflt1 efm eflt2 efr

export
merge3 :  {ctx: TCtx} -> {a,b,c,d,e,f: ctx.key}
       -> Tree23 ctx a b (S depth) -> (0 bc: ctx.rel b c)
       -> Tree23 ctx c d (S depth) -> (0 de: ctx.rel d e)
       -> Tree23 ctx e f depth
       -> Tree23 ctx a f (S (S depth)) 
merge3 (Branch2 abl abr {r0=ablt}) b_lt_c (Branch2 cdl cdr {r0=cdlt1}) d_lt_e ef =
  branch5 abl ablt abr b_lt_c cdl cdlt1 cdr d_lt_e ef
merge3 (Branch2 abl abr {r0=ablt}) b_lt_c (Branch3 cdl cdm cdr {r1=cdlt1} {r2=cdlt2}) d_lt_e ef =
  branch6 abl ablt abr b_lt_c cdl cdlt1 cdm cdlt2 cdr d_lt_e ef
merge3 (Branch3 abl abm abr {r1=ablt1} {r2=ablt2}) b_lt_c (Branch2 cdl cdr {r0=cdlt1}) d_lt_e ef =
  branch6 abl ablt1 abm ablt2 abr b_lt_c cdl cdlt1 cdr d_lt_e ef
merge3 (Branch3 abl abm abr {r1=ablt1} {r2=ablt2}) b_lt_c (Branch3 cdl cdm cdr {r1=cdlt1} {r2=cdlt2}) d_lt_e ef =
  branch7 abl ablt1 abm ablt2 abr b_lt_c cdl cdlt1 cdm cdlt2 cdr d_lt_e ef
