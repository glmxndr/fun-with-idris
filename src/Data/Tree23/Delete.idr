module Data.Tree23.Delete

import public Data.Tree23.Types
import public Data.Tree23.Lemmas
import public Data.Tree23.DecElem
import public Data.Tree23.Merge
import public Data.Tree23.Bounds

%default total
%ambiguity_depth 10
%search_timeout 5000

public export
data Deleted : {ctx: TCtx}
                   -> {a,f: ctx.key}
                   -> (k : ctx.key)
                   -> (0 t : Tree23 ctx a f depth)
                   -> Type where
  DeletedLeaf: {ctx: TCtx}
            -> (k : ctx.key)
            -> {0 v: ctx.val k}
            -> Deleted {ctx,a=k,f=k} k (Leaf k v)

  SameBounds:  {ctx: TCtx}
            -> {a,f: ctx.key}
            -> (k : ctx.key)
            -> (0 a_lt_k: ctx.rel a k)
            -> (0 k_lt_f: ctx.rel k f)
            -> (0 t : Tree23 ctx a f depth)
            -> (t': Tree23 ctx a f depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a,f} k t

  SameBoundsShallower:  {ctx: TCtx}
            -> {a,f: ctx.key}
            -> (k : ctx.key)
            -> (0 a_lt_k: ctx.rel a k)
            -> (0 k_lt_f: ctx.rel k f)
            -> (0 t : Tree23 ctx a f (S depth))
            -> (t': Tree23 ctx a f depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a,f} k t

  LowBound:  {ctx: TCtx}
            -> {a',f,k: ctx.key}
            -> (0 k_lt_a': ctx.rel k a')
            -> (0 t: Tree23 ctx k  f depth)
            -> (t': Tree23 ctx a' f depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a=k,f} k t

  LowBoundShallower:  {ctx: TCtx}
            -> {a',f,k: ctx.key}
            -> (0 k_lt_a': ctx.rel k a')
            -> (0 t : Tree23 ctx k f (S depth))
            -> (t': Tree23 ctx a' f depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a=k,f} k t


  HighBound:  {ctx: TCtx}
            -> {a,f',k: ctx.key}
            -> (0 f'_lt_k: ctx.rel f' k)
            -> (0 t : Tree23 ctx a k depth)
            -> (t': Tree23 ctx a f' depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a,f=k} k t

  HighBoundShallower:  {ctx: TCtx}
            -> {a,f',k: ctx.key}
            -> (0 f'_lt_k: ctx.rel f' k)
            -> (0 t : Tree23 ctx a k (S depth))
            -> (t': Tree23 ctx a f' depth)
            -> (0 ne: Not (Elem k t'))
            -> Deleted {ctx,a,f=k} k t



lemma_not_elem_trans : {ctx: TCtx}
                     -> {a,b,c,f,k: ctx.key}
                     -> {l : Tree23 ctx a b depth}
                     -> {r : Tree23 ctx c f depth}
                     -> {0 r0 : ctx.rel b c}
                     -> ctx.rel k c
                     -> Not (Elem k l)
                     -> Not (Elem k (Branch2 l r))
                     

public export
treeDelete :  {ctx: TCtx} -> {a,f: ctx.key} -> {0 depth: Nat}
            -> (k: ctx.key)
            -> (t: Tree23 ctx a f depth)
            -> (elem: Elem k t)
            -> Deleted {ctx,a,f} k t 
treeDelete a (Leaf a v) InLeaf = DeletedLeaf a
treeDelete k (Branch2 {r0} l r) (InB2L eleml) with (treeDelete k l eleml)

  treeDelete k (Branch2 {r0} (Leaf k v) r) (InB2L eleml) | (DeletedLeaf k) = 
    LowBoundShallower r0 _ r (lemma_too_low r r0)
      
  treeDelete k (Branch2 {r0} l r) (InB2L eleml) | (SameBounds k altk kltf l t' g) = 
    SameBounds 
      k 
      altk 
      (kltf !<! r0 !<~ r) 
      _ 
      (Branch2 t' r) 
      (lemma_not_elem_trans (kltf !<! r0) g)

  treeDelete k (Branch2 {r0} l r@(Branch2 {r0=rr0} rl rr)) elem@(InB2L eleml) | (SameBoundsShallower k altk kltf l t' g) =
    SameBoundsShallower 
      k 
      altk 
      (kltf !<! r0 !<~ r) 
      _ 
      (Branch3 t' rl rr)
      (lemma_b3_not_in_lmr 
        g 
        (lemma_too_low rl (kltf !<! r0)) 
        (lemma_too_low rr (kltf !<! (r0 !<~ rl) !<! rr0))
      )
      
  treeDelete k (Branch2 {r0} l r@(Branch3 {r1,r2} rl rm rr)) elem@(InB2L eleml) | (SameBoundsShallower k altk kltf l t' g) =
    SameBounds 
      k 
      altk 
      (kltf !<! r0 !<~ r) 
      _
      (Branch2 (Branch2 t' rl) (Branch2 rm rr)) 
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr 
          g 
          (lemma_too_low rl (kltf !<! r0)))
        (lemma_b2_not_in_lr 
          (lemma_too_low rm (kltf !<! (r0 !<~ rl) !<! r1)) 
          (lemma_too_low rr (kltf !<! (r0 !<~ rl) !<! r1 !<! (rm ~<! r2))))
      )
    
  treeDelete k (Branch2 {r0} l r) (InB2L eleml) | (LowBound lt l t' g) =
    LowBound lt _ (Branch2 t' r) (lemma_b2_not_in_lr g (lemma_too_low r (lt !<! t' ~<! r0)))
    
  treeDelete k (Branch2 {r0} l (Branch2 {r0=rr0} rl rr)) (InB2L eleml) | (LowBoundShallower lt l t' g) =
    LowBoundShallower 
      lt 
      _ 
      (Branch3 t' rl rr) 
      (lemma_b3_not_in_lmr
        g
        (lemma_too_low rl (lt !<! t' ~<! r0))
        (lemma_too_low rr (lt !<! t' ~<! r0 !<! rl ~<! rr0))
      )
    
  treeDelete k (Branch2 {r0} l (Branch3 {r1,r2} rl rm rr)) (InB2L eleml) | (LowBoundShallower lt l t' g) =
    LowBound 
      lt 
      _ 
      (Branch2 (Branch2 t' rl) (Branch2 rm rr))
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr 
          g 
          (lemma_too_low rl (lt !<! t' ~<! r0))
        )
        (lemma_b2_not_in_lr 
          (lemma_too_low rm (lt !<! t' ~<! r0 !<! rl ~<! r1))
          (lemma_too_low rr (lt !<! t' ~<! r0 !<! rl ~<! r1 !<! rm ~<! r2))
        )
      ) 

  treeDelete k (Branch2 {r0} l r) (InB2L eleml) | (HighBound lt l t' g) =
    SameBounds 
      k 
      (trans_tree_lt t' lt) 
      (trans_lt_tree r0 r) 
      _ 
      (Branch2 {r0 = lt !<! r0} t' r) 
      (lemma_b2_not_in_lr
        g
        (lemma_too_low r r0)
      )
    
  treeDelete k (Branch2 {r0} l r@(Branch2 {r0=rr0} rl rr)) (InB2L eleml) | (HighBoundShallower lt l t' g) =
    SameBoundsShallower
      k
      (lemma_treeS_lt l)
      (trans_lt_tree r0 r)
      _
      (Branch3 {r1 = lt !<! r0} t' rl rr)
      (lemma_b3_not_in_lmr
        g
        (lemma_too_low rl r0)
        (lemma_too_low rr (r0 !<! rl ~<! rr0))
      )
  
  treeDelete k (Branch2 {r0} l r@(Branch3 {r1,r2} rl rm rr)) (InB2L eleml) | (HighBoundShallower lt l t' g) =
    SameBounds
      k
      (lemma_treeS_lt l)
      (trans_lt_tree r0 r)
      _
      (Branch2 (Branch2 t' {r0 = lt !<! r0} rl) (Branch2 rm rr))
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr 
          g
          (lemma_too_low rl r0)
        )
        (lemma_b2_not_in_lr 
          (lemma_too_low rm (r0 !<! rl ~<! r1))
          (lemma_too_low rr (r0 !<! rl ~<! r1 !<! rm ~<! r2))
        )
      )

treeDelete k (Branch2 {r0} l r) (InB2R elemr) with (treeDelete k r elemr)
  treeDelete k (Branch2 {r0} l (Leaf k v)) (InB2R elemr) | (DeletedLeaf k) =
    HighBoundShallower r0 _ l (lemma_too_high l r0)
  
  treeDelete k (Branch2 {r0} l r) (InB2R elemr) | (SameBounds k a_lt_k k_lt_f r t' ne) =
    SameBounds 
      k 
      (l ~<! r0 !<! a_lt_k) 
      k_lt_f 
      _ 
      (Branch2 l t') 
      (lemma_b2_not_in_lr
        (lemma_too_high l (r0 !<! a_lt_k))
        ne
      )

  treeDelete k (Branch2 {r0} l@(Branch2 {r0=lr0} ll lr) r) (InB2R elemr) | (SameBoundsShallower k a_lt_k k_lt_f r t' ne) =
    SameBoundsShallower
      k
      (l ~<! r0 !<! a_lt_k)
      k_lt_f
      _
      (Branch3 ll lr t')
      (lemma_b3_not_in_lmr
        (lemma_too_high ll (lr0 !<! lr ~<! r0 !<! a_lt_k))
        (lemma_too_high lr (r0 !<! a_lt_k))
        ne
      )
  
  treeDelete k (Branch2 {r0} l@(Branch3 {r1,r2} ll lm lr) r) (InB2R elemr) | (SameBoundsShallower k a_lt_k k_lt_f r t' ne) =
    SameBounds
      k
      (l ~<! r0 !<! a_lt_k)
      k_lt_f
      _ 
      (Branch2 (Branch2 ll lm) (Branch2 lr t'))
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr 
          (lemma_too_high ll (r1 !<! lm ~<! r2 !<! lr ~<! r0 !<! a_lt_k))
          (lemma_too_high lm (r2 !<! lr ~<! r0 !<! a_lt_k))
        )
        (lemma_b2_not_in_lr 
          (lemma_too_high lr (r0 !<! a_lt_k))
          ne
        )
      )

  treeDelete k (Branch2 {r0} l r) (InB2R elemr) | (LowBound k_lt_a' r t' ne) =
    SameBounds
      k
      (l ~<! r0)
      (k_lt_a' !<~ t')
      _
      (Branch2 {r0 = r0 !<! k_lt_a'} l t')
      (lemma_b2_not_in_lr
        (lemma_too_high l r0)
        ne
      )
  
  treeDelete k (Branch2 {r0} l@(Branch2 {r0=lr0} ll lr) r) (InB2R elemr) | (LowBoundShallower k_lt_a' r t' ne) =
    SameBoundsShallower
      k
      (l ~<! r0)
      (k_lt_a' !<~ t')
      _
      (Branch3 {r2= r0 !<! k_lt_a'} ll lr t')
      (lemma_b3_not_in_lmr
        (lemma_too_high ll (lr0 !<! lr ~<! r0))
        (lemma_too_high lr r0)
        ne
      )
  
  treeDelete k (Branch2 {r0} l@(Branch3 {r1,r2} ll lm lr) r) (InB2R elemr) | (LowBoundShallower k_lt_a' r t' ne) =
    SameBounds
      k
      (l ~<! r0)
      (k_lt_a' !<~ t')
      _
      (Branch2 
        (Branch2 ll lm)
        (Branch2 {r0=r0 !<! k_lt_a'} lr t')
      )
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr
          (lemma_too_high ll (r1 !<! lm ~<! r2 !<! lr ~<! r0))
          (lemma_too_high lm (r2 !<! lr ~<! r0))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high lr r0)
          ne
        )
      )
  
  treeDelete k (Branch2 {r0} l r) (InB2R elemr) | (HighBound f'_lt_k r t' ne) =
    HighBound
      f'_lt_k
      _
      (Branch2 l t')
      (lemma_b2_not_in_lr
        (lemma_too_high l (r0 !<~ r))
        ne)
  
  treeDelete k (Branch2 {r0} (Branch2 {r0=lr0} ll lr) r) (InB2R elemr) | (HighBoundShallower f'_lt_k r t' ne) =
    HighBoundShallower
      f'_lt_k
      _
      (Branch3 ll lr t')
      (lemma_b3_not_in_lmr
        (lemma_too_high ll (lr0 !<! lr ~<! r0 !<~ r))
        (lemma_too_high lr (r0 !<~ r))
        ne
      )
  
  treeDelete k (Branch2 {r0} (Branch3 {r1,r2} ll lm lr) r) (InB2R elemr) | (HighBoundShallower f'_lt_k r t' ne) =
    HighBound
      f'_lt_k
      _
      (Branch2
        (Branch2 ll lm)
        (Branch2 lr t')
      )
      (lemma_b2_not_in_lr
        (lemma_b2_not_in_lr
          (lemma_too_high ll (r1 !<! lm ~<! r2 !<! lr ~<! r0 !<~ r))
          (lemma_too_high lm (r2 !<! lr ~<! r0 !<~ r))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high lr (r0 !<~ r))
          ne
        )
      )
      

treeDelete k (Branch3 {r1,r2} l m r) (InB3L eleml) with (treeDelete k l eleml)
  treeDelete k (Branch3 {r1,r2} (Leaf k v) m r) (InB3L eleml) | (DeletedLeaf k) =
    LowBound
      r1
      _
      (Branch2 m r)
      (lemma_b2_not_in_lr
        (lemma_too_low m r1)
        (lemma_too_low r (r1 !<! m ~<! r2))
      )
      
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3L eleml) | (SameBounds k a_lt_k k_lt_f l t' ne) =
    SameBounds
      k
      a_lt_k
      (k_lt_f !<! r1 !<! m ~<! r2 !<~ r)
      _
      (Branch3 t' m r)
      (lemma_b3_not_in_lmr 
        ne
        (lemma_too_low m (k_lt_f !<! r1))
        (lemma_too_low r (k_lt_f !<! r1 !<! m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m@(Branch2 {r0=mr0} ml mr) r) (InB3L eleml) | (SameBoundsShallower k a_lt_k k_lt_f l t' ne) =
    SameBounds
      k
      a_lt_k
      (k_lt_f !<! r1 !<! m ~<! r2 !<~ r)
      _
      (Branch2 (Branch3 t' ml mr) r)
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low ml (k_lt_f !<! r1))
          (lemma_too_low mr (k_lt_f !<! r1 !<! ml ~<! mr0))
        )
        (lemma_too_low r (k_lt_f !<! r1 !<! m ~<! r2))
      )
  
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) 
      r@(Branch2 {r0=rr0} rl rr)
    ) 
    (InB3L eleml) | (SameBoundsShallower k a_lt_k k_lt_f l t' ne) =
    SameBounds
      k
      a_lt_k
      (k_lt_f !<! r1 !<! m ~<! r2 !<~ r)
      _
      (Branch2 (Branch3 t' ml mm) (Branch3 mr rl rr))
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low ml (k_lt_f !<! r1))
          (lemma_too_low mm (k_lt_f !<! r1 !<! ml ~<! mr1))
        )
        (lemma_b3_not_in_lmr
          (lemma_too_low mr (k_lt_f !<! r1 !<! ml ~<! mr1 !<! mm ~<! mr2))
          (lemma_too_low rl (k_lt_f !<! r1 !<! m ~<! r2))
          (lemma_too_low rr (k_lt_f !<! r1 !<! m ~<! r2 !<! rl ~<! rr0))
        )
      )
    
  
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) 
      r@(Branch3 {r1=rr1,r2=rr2} rl rm rr)
    ) 
    (InB3L eleml) | (SameBoundsShallower k a_lt_k k_lt_f l t' ne) =
    SameBounds
      k
      a_lt_k
      (k_lt_f !<! r1 !<! m ~<! r2 !<~ r)
      _
      (Branch3 (Branch2 t' ml) (Branch3 mm mr rl) (Branch2 rm rr))
      (lemma_b3_not_in_lmr
        (lemma_b2_not_in_lr
          ne
          (lemma_too_low ml (k_lt_f !<! r1))
        )
        (lemma_b3_not_in_lmr
          (lemma_too_low mm (k_lt_f !<! r1 !<! ml ~<! mr1))
          (lemma_too_low mr (k_lt_f !<! r1 !<! ml ~<! mr1 !<! mm ~<! mr2))
          (lemma_too_low rl (k_lt_f !<! r1 !<! m ~<! r2))
        )
        (lemma_b2_not_in_lr
          (lemma_too_low rm (k_lt_f !<! r1 !<! m ~<! r2 !<! rl ~<! rr1))
          (lemma_too_low rr (k_lt_f !<! r1 !<! m ~<! r2 !<! rl ~<! rr1 !<! rm ~<! rr2))
        )
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3L eleml) | (LowBound k_lt_a' l t' ne) =
    LowBound
      k_lt_a'
      _
      (Branch3 t' m r)
      (lemma_b3_not_in_lmr 
        ne
        (lemma_too_low m (k_lt_a' !<! t' ~<! r1))
        (lemma_too_low r (k_lt_a' !<! t' ~<! r1 !<! m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m@(Branch2 {r0} ml mr) r) (InB3L eleml) | (LowBoundShallower k_lt_a' l t' ne) =
    LowBound
      k_lt_a'
      _ 
      (Branch2 (Branch3 t' ml mr) r)
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low ml (k_lt_a' !<! t' ~<! r1))
          (lemma_too_low mr (k_lt_a' !<! t' ~<! r1 !<! ml ~<! r0))
        )
        (lemma_too_low r (k_lt_a' !<! t' ~<! r1 !<! m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) r) (InB3L eleml) | (LowBoundShallower k_lt_a' l t' ne) =
    LowBound
      k_lt_a'
      _ 
      (Branch3 (Branch2 t' ml) (Branch2 mm mr) r)
      (lemma_b3_not_in_lmr
        (lemma_b2_not_in_lr
          ne
          (lemma_too_low ml (k_lt_a' !<! t' ~<! r1))
        )
        (lemma_b2_not_in_lr
          (lemma_too_low mm (k_lt_a' !<! t' ~<! r1 !<! ml ~<! mr1))
          (lemma_too_low mr (k_lt_a' !<! t' ~<! r1 !<! ml ~<! mr1 !<! mm ~<! mr2))
        )
        (lemma_too_low r (k_lt_a' !<! t' ~<! r1 !<! m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3L eleml) | (HighBound f'_lt_k l t' ne) =
    SameBounds
      k
      (t' ~<! f'_lt_k)
      (r1 !<! m ~<! r2 !<~ r)
      _
      (Branch3 {r1= f'_lt_k !<! r1} t' m r)
      (lemma_b3_not_in_lmr
        ne
        (lemma_too_low m r1)
        (lemma_too_low r (r1 !<! m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m@(Branch2 {r0=mr0} ll lr) r) (InB3L eleml) | (HighBoundShallower f'_lt_k l t' ne) =
    SameBounds
      k
      (t' ~<! f'_lt_k)
      (r1 !<! m ~<! r2 !<~ r)
      _
      (Branch2 (Branch3 {r1= f'_lt_k !<! r1} t' ll lr) r)
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low ll r1)
          (lemma_too_low lr (r1 !<! ll ~<! mr0))
        )
        (lemma_too_low r (r1 !<! m ~<! r2))
      )
      
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ll lm lr) 
      r@(Branch2 {r0=rr0} rl rr)
    ) 
    (InB3L eleml) | (HighBoundShallower f'_lt_k l t' ne) =
      SameBounds
        k
        (t' ~<! f'_lt_k)
        (r1 !<! m ~<! r2 !<~ r)
        _
        (Branch2 (Branch3 {r1= f'_lt_k !<! r1} t' ll lm) (Branch3 lr rl rr))
        (lemma_b2_not_in_lr
          (lemma_b3_not_in_lmr
            ne
            (lemma_too_low ll r1)
            (lemma_too_low lm (r1 !<! ll ~<! mr1))
          )
          (lemma_b3_not_in_lmr
            (lemma_too_low lr (r1 !<! ll ~<! mr1 !<! lm ~<! mr2))
            (lemma_too_low rl (r1 !<! m ~<! r2))
            (lemma_too_low rr (r1 !<! m ~<! r2 !<! rl ~<! rr0))
          )
        )
      
  
  treeDelete 
    k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ll lm lr) 
      r@(Branch3 {r1=rr1,r2=rr2} rl rm rr)
    ) 
    (InB3L eleml) | (HighBoundShallower f'_lt_k l t' ne) =
    SameBounds 
      k
      (t' ~<! f'_lt_k)
      (r1 !<! m ~<! r2 !<~ r)
      _
      (Branch3 (Branch2 {r0= f'_lt_k !<! r1} t' ll) (Branch3 lm lr rl) (Branch2 rm rr))
      (lemma_b3_not_in_lmr
        (lemma_b2_not_in_lr
          ne
          (lemma_too_low ll r1)
         
        )
        (lemma_b3_not_in_lmr
          (lemma_too_low lm (r1 !<! ll ~<! mr1))
          (lemma_too_low lr (r1 !<! ll ~<! mr1 !<! lm ~<! mr2))
          (lemma_too_low rl (r1 !<! m ~<! r2))
        )
        (lemma_b2_not_in_lr
          (lemma_too_low rm (r1 !<! m ~<! r2 !<! rl ~<! rr1))
          (lemma_too_low rr (r1 !<! m ~<! r2 !<! rl ~<! rr1 !<! rm ~<! rr2))
        )
      )
 

treeDelete k (Branch3 {r1,r2} l m r) (InB3M elemm) with (treeDelete k m elemm)
  treeDelete k (Branch3 {r1,r2} l (Leaf k v) r) (InB3M elemm) | (DeletedLeaf k) =
    SameBounds
      k
      (l ~<! r1)
      (r2 !<~ r)
      _
      (Branch2 {r0=r1 !<! r2} l r)
      (lemma_b2_not_in_lr
        (lemma_too_high l r1)
        (lemma_too_low r r2)
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3M elemm) | (SameBounds k a_lt_k k_lt_f m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! a_lt_k)
      (k_lt_f !<! r2 !<~ r)
      _
      (Branch3 l t' r)
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! a_lt_k))
        ne
        (lemma_too_low r (k_lt_f !<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l@(Branch2 {r0=lr0} ll lr) m r) (InB3M elemm) | (SameBoundsShallower k a_lt_k k_lt_f m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! a_lt_k) 
      (k_lt_f !<! r2 !<~ r)
      _
      (Branch2 (Branch3 ll lr t') r)
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          (lemma_too_high ll (lr0 !<! lr ~<! r1 !<! a_lt_k))
          (lemma_too_high lr (r1 !<! a_lt_k))
          ne
        )
        (lemma_too_low r (k_lt_f !<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m r@(Branch2 {r0=rr0} rl rr)) (InB3M elemm) | (SameBoundsShallower k a_lt_k k_lt_f m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! a_lt_k) 
      (k_lt_f !<! r2 !<~ r)
      _
      (Branch2 l (Branch3 t' rl rr))
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<! a_lt_k))
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low rl (k_lt_f !<! r2))
          (lemma_too_low rr (k_lt_f !<! r2 !<! rl ~<! rr0))
        )
      )
  
  treeDelete k (Branch3 {r1,r2} l m r@(Branch3 {r1=rr1,r2=rr2} rl rm rr)) (InB3M elemm) | (SameBoundsShallower k a_lt_k k_lt_f m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! a_lt_k)
      (k_lt_f !<! r2 !<~ r)
      _
      (Branch3 l (Branch2 t' rl) (Branch2 rm rr))
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! a_lt_k))
        (lemma_b2_not_in_lr
          ne
          (lemma_too_low rl (k_lt_f !<! r2))
        )
        (lemma_b2_not_in_lr
          (lemma_too_low rm (k_lt_f !<! r2 !<! rl ~<! rr1))
          (lemma_too_low rr (k_lt_f !<! r2 !<! rl ~<! rr1 !<! rm ~<! rr2))
        )
      )    
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3M elemm) | (LowBound k_lt_a' m t' ne) =
    SameBounds
      k
      (l ~<! r1)
      (k_lt_a' !<! t' ~<! r2 !<~ r)
      _
      (Branch3 {r1=r1 !<! k_lt_a'} l t' r)
      (lemma_b3_not_in_lmr
        (lemma_too_high l r1)
        ne
        (lemma_too_low r (m ~<! r2))
      )
    
  treeDelete k (Branch3 {r1,r2} l@(Branch2 {r0=lr0} ll lr) m r) (InB3M elemm) | (LowBoundShallower k_lt_a' m t' ne) =
    SameBounds
      k
      (l ~<! r1)
      (m ~<! r2 !<~ r)
      _
      (Branch2 (Branch3 {r2= r1 !<! k_lt_a'} ll lr t') r)
      (lemma_b2_not_in_lr
        (lemma_b3_not_in_lmr
          (lemma_too_high ll (lr0 !<! lr ~<! r1))
          (lemma_too_high lr r1)
          ne
        )
        (lemma_too_low r (m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l@(Branch3 {r1=lr1,r2=lr2} ll lm lr) m r) (InB3M elemm) | (LowBoundShallower k_lt_a' m t' ne) =
    SameBounds
      k
      (l ~<! r1)
      (m ~<! r2 !<~ r)
      _
      (Branch3 (Branch2 ll lm) (Branch2 {r0=r1 !<! k_lt_a'} lr t') r)
      (lemma_b3_not_in_lmr
        (lemma_b2_not_in_lr
          (lemma_too_high ll (lr1 !<! lm ~<! lr2 !<! lr ~<! r1))
          (lemma_too_high lm (lr2 !<! lr ~<! r1))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high lr r1)
          ne
        )
        (lemma_too_low r (m ~<! r2))
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3M elemm) | (HighBound f'_lt_k m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<~ m)
      (r2 !<~ r)
      _
      (Branch3 {r2=f'_lt_k !<! r2} l t' r)
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<~ m))
        ne
        (lemma_too_low r r2)
      )
  
  treeDelete k (Branch3 {r1,r2} l m r@(Branch2 {r0=rr0} rl rr)) (InB3M elemm) | (HighBoundShallower f'_lt_k m t' ne) =
    SameBounds
      k
      (l ~<! r1 !<~ m)
      (r2 !<~ r)
      _
      (Branch2 l (Branch3 {r1=f'_lt_k !<! r2} t' rl rr))
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<~ m))
        (lemma_b3_not_in_lmr
          ne
          (lemma_too_low rl r2)
          (lemma_too_low rr (r2 !<! rl ~<! rr0))
        )
      )
      
  treeDelete k 
    (Branch3 {r1,r2} 
      l@(Branch2 {r0=lr0} ll lr) 
      m 
      r@(Branch3 {r1=rr1,r2=rr2} rl rm rr)
    ) 
    (InB3M elemm) | (HighBoundShallower f'_lt_k m t' ne) =
      SameBounds
        k
        (l ~<! r1 !<~ m)
        (r2 !<~ r)
        _
        (Branch2 {r0=f'_lt_k !<! r2} (Branch3 ll lr t') r)
        (lemma_b2_not_in_lr
          (lemma_b3_not_in_lmr
            (lemma_too_high ll (lr0 !<! lr ~<! r1 !<~ m))
            (lemma_too_high lr (r1 !<~ m))
            ne
          )
          (lemma_too_low r r2)
        )
    
  
  treeDelete k 
    (Branch3 {r1,r2} 
      l@(Branch3 {r1=lr1,r2=lr2} ll lm lr) 
      m 
      r@(Branch3 {r1=rr1,r2=rr2} rl rm rr)) 
    (InB3M elemm) | (HighBoundShallower f'_lt_k m t' ne) =
      SameBounds
        k
        (l ~<! r1 !<~ m)
        (r2 !<~ r)
        _
        (Branch3
          (Branch2 ll lm)
          (Branch3 {r2=f'_lt_k !<! r2} lr t' rl)
          (Branch2 rm rr)
        )
        (lemma_b3_not_in_lmr
          (lemma_b2_not_in_lr
            (lemma_too_high ll (lr1 !<! lm ~<! lr2 !<! lr ~<! r1 !<~ m))
            (lemma_too_high lm (lr2 !<! lr ~<! r1 !<~ m))
          )
          (lemma_b3_not_in_lmr
            (lemma_too_high lr (r1 !<~ m))
            ne
            (lemma_too_low rl r2)
          )
          (lemma_b2_not_in_lr
            (lemma_too_low rm (r2 !<! rl ~<! rr1))
            (lemma_too_low rr (r2 !<! rl ~<! rr1 !<! rm ~<! rr2))
          )
        
        )

treeDelete k (Branch3 {r1,r2} l m r) (InB3R elemr) with (treeDelete k r elemr)
  treeDelete k (Branch3 {r1,r2} l m (Leaf k v)) (InB3R elemr) | (DeletedLeaf k) =
    HighBound
      r2
      _
      (Branch2 l m)
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<! m ~<! r2))
        (lemma_too_high m r2)
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3R elemr) | (SameBounds k a_lt_k k_lt_f r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2 !<! a_lt_k)
      k_lt_f
      _
      (Branch3 l m t')
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! m ~<! r2 !<! a_lt_k))
        (lemma_too_high m (r2 !<! a_lt_k))
        ne
      )
  
  treeDelete k
    (Branch3 {r1,r2}
      l 
      m@(Branch2 {r0=mr0} ml mr) 
      r
    ) 
    (InB3R elemr) | (SameBoundsShallower k a_lt_k k_lt_f r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2 !<! a_lt_k)
      k_lt_f
      _
      (Branch2 l (Branch3 ml mr t'))
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<! m ~<! r2 !<! a_lt_k))
        (lemma_b3_not_in_lmr
          (lemma_too_high ml (mr0 !<! mr ~<! r2 !<! a_lt_k))
          (lemma_too_high mr (r2 !<! a_lt_k))
          ne
        )
      )
    
  treeDelete k
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) 
      r
    ) 
    (InB3R elemr) | (SameBoundsShallower k a_lt_k k_lt_f r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2 !<! a_lt_k)
      k_lt_f
      _
      (Branch3 l (Branch2 ml mm) (Branch2 mr t'))
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! m ~<! r2 !<! a_lt_k))
        (lemma_b2_not_in_lr
          (lemma_too_high ml (mr1 !<! mm ~<! mr2 !<! mr ~<! r2 !<! a_lt_k))
          (lemma_too_high mm (mr2 !<! mr ~<! r2 !<! a_lt_k))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high mr (r2 !<! a_lt_k))
          ne
        )
      )
    
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3R elemr) | (LowBound k_lt_a' r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2)
      (k_lt_a' !<~ t')
      _
      (Branch3 {r2=r2 !<! k_lt_a'} l m t')
      (lemma_b3_not_in_lmr 
        (lemma_too_high l (r1 !<! m ~<! r2))
        (lemma_too_high m r2)
        ne
      )
  
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch2 {r0=mr0} ml mr) 
      r
    ) 
    (InB3R elemr) | (LowBoundShallower k_lt_a' r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2)
      (k_lt_a' !<~ t')
      _
      (Branch2 l (Branch3 {r2=r2 !<! k_lt_a'} ml mr t'))
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<! m ~<! r2))
        (lemma_b3_not_in_lmr
          (lemma_too_high ml (mr0 !<! mr ~<! r2))
          (lemma_too_high mr r2)
          ne
        )
      )
    
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) 
      r
    ) 
    (InB3R elemr) | (LowBoundShallower k_lt_a' r t' ne) =
    SameBounds
      k
      (l ~<! r1 !<! m ~<! r2)
      (k_lt_a' !<~ t')
      _
      (Branch3 l (Branch2 ml mm) (Branch2 {r0=r2 !<! k_lt_a'} mr t'))
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! m ~<! r2))
        (lemma_b2_not_in_lr
          (lemma_too_high ml (mr1 !<! mm ~<! mr2 !<! mr ~<! r2))
          (lemma_too_high mm (mr2 !<! mr ~<! r2))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high mr r2)
          ne
        )
      )
  
  treeDelete k (Branch3 {r1,r2} l m r) (InB3R elemr) | (HighBound f'_lt_k r t' ne) =
    HighBound
      f'_lt_k
      _
      (Branch3 l m t')
      (lemma_b3_not_in_lmr 
        (lemma_too_high l (r1 !<! m ~<! r2 !<~ r))
        (lemma_too_high m (r2 !<~ r))
        ne
      )
  
  
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch2 {r0=mr0} ml mr)
      r
    ) 
    (InB3R elemr) | (HighBoundShallower f'_lt_k r t' ne) =
    HighBound
      f'_lt_k
      _
      (Branch2 l (Branch3 ml mr t'))
      (lemma_b2_not_in_lr
        (lemma_too_high l (r1 !<! m ~<! r2 !<~ r))
        (lemma_b3_not_in_lmr
          (lemma_too_high ml (mr0 !<! mr ~<! r2 !<~ r))
          (lemma_too_high mr (r2 !<~ r))
          ne
        )
      )

    
  treeDelete k 
    (Branch3 {r1,r2} 
      l 
      m@(Branch3 {r1=mr1,r2=mr2} ml mm mr) 
      r
    ) 
    (InB3R elemr) | (HighBoundShallower f'_lt_k r t' ne) =
    HighBound
      f'_lt_k
      _
      (Branch3 l (Branch2 ml mm) (Branch2 mr t'))
      (lemma_b3_not_in_lmr
        (lemma_too_high l (r1 !<! m ~<! r2 !<~ r))
        (lemma_b2_not_in_lr
          (lemma_too_high ml (mr1 !<! mm ~<! mr2 !<! mr ~<! r2 !<~ r))
          (lemma_too_high mm (mr2 !<! mr ~<! r2 !<~ r))
        )
        (lemma_b2_not_in_lr
          (lemma_too_high mr (r2 !<~ r))
          ne
        )
      )

