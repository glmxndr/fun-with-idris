module Data.Nat.Primality.Lemmas

import Data.Nat.Divides
import Data.Nat.LessThan

import Syntax.PreorderReasoning


%default total


public export
plusiSj_eq_Splusij : (i,j:Nat) -> i + S j = S (i + j)
plusiSj_eq_Splusij 0 j = Refl
plusiSj_eq_Splusij (S k) j = Calc $
  |~ S (k + S j)
  ~~ S (S (k + j)) ... (cong S $ plusiSj_eq_Splusij k j)

public export
uncongNat : (n:Nat) -> n+x=n+y -> x=y
uncongNat 0 prf = prf
uncongNat (S k) prf = uncongNat k $ eqSxSyeqxy prf
