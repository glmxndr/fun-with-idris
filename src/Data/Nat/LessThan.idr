module Data.Nat.LessThan

import public Data.Nat
import public Decidable.Equality
import public Decidable.Order.Strict
import public Control.Relation
import Syntax.PreorderReasoning
import Syntax.PreorderReasoning.Generic

%default total

infixr 10 .<.

public export
data (.<.) : Nat -> Nat -> Type where
  Below : Z .<. S a
  Less  : a .<. b -> S a .<. S b

threeLessThanSeven : 3 .<. 7
threeLessThanSeven = Less (Less (Less Below))

export
sltIrreflexive : {a:Nat} -> Not (a .<. a)
sltIrreflexive (Less x) = sltIrreflexive x

export
sltNoSaLTa : {a:Nat} -> Not (S a .<. a)
sltNoSaLTa (Less x) = sltNoSaLTa x

export
sltAsymmetric : {a,b:Nat} -> a .<. b -> Not (b .<. a)
sltAsymmetric Below Below impossible
sltAsymmetric Below (Less x) impossible
sltAsymmetric (Less x) (Less y) = sltAsymmetric x y

export
sltTransitive : {a,b:Nat} -> a .<. b -> b .<. c -> a .<. c
sltTransitive Below (Less x) = Below
sltTransitive (Less x) (Less y) = Less (sltTransitive x y)

export
sltConnected : {a,b:Nat} -> Not (a=b) -> Either (a .<. b) (b .<. a)
sltConnected {a=Z} {b=Z} notaeqb = absurd $ notaeqb Refl 
sltConnected {a=Z} {b=S x} notaeqb = Left Below
sltConnected {a=S x} {b=Z} notaeqb = Right Below
sltConnected {a=S x} {b=S y} notaeqb with (decEq x y)
  sltConnected {a=S x} {b=S y} notaeqb | Yes xeqy = absurd $ (notaeqb $ cong S xeqy)
  sltConnected {a=S x} {b=S y} notaeqb | No notxeqy with (sltConnected notxeqy)
    sltConnected {a=S x} {b=S y} notaeqb | No notxeqy | Left xlty  = Left  (Less xlty)
    sltConnected {a=S x} {b=S y} notaeqb | No notxeqy | Right yltx = Right (Less yltx)

export
Irreflexive Nat (.<.) where
  irreflexive = sltIrreflexive

export
Asymmetric Nat (.<.) where
  asymmetric = sltAsymmetric

export
Transitive Nat (.<.) where
  transitive = sltTransitive

public export
interface Connected ty rel | rel where
  constructor MkConnected
  connected : {x, y : ty} -> Not (x=y) -> Either (rel x y) (rel y x)

export
Connected Nat (.<.) where
  connected = sltConnected

export
ltNotEq : a .<. b -> Not (a = b)
ltNotEq Below Refl impossible
ltNotEq (Less x) Refl = ltNotEq x Refl


%hint
export
multn0eq0 : {n : Nat} -> n * Z = Z
multn0eq0 {n=Z}   = Refl
multn0eq0 {n=S k} = multn0eq0 {n=k}

%hint
export
plusn0eqn : {n:Nat} -> n + 0 = n
plusn0eqn {n=Z} = Refl
plusn0eqn {n=S m} = cong S (plusn0eqn {n=m})

export
sltS : {a,b:Nat} -> a .<. b -> a .<. S b
sltS Below = Below
sltS (Less x) = Less (sltS x)

export
sltAdd : {a,b,c:Nat} -> a .<. b -> a .<. (b + c)
sltAdd {c = 0} x = rewrite plusn0eqn {n=b} in x
sltAdd {a = 0} {c = (S k)} Below = Below
sltAdd {a = (S j)} {c = (S k)} (Less x) = Less (sltAdd x)

export
sltAdd' : {a,b,c:Nat} -> a .<. b -> a .<. (c + b)
sltAdd' altb = rewrite plusCommutative c b in sltAdd altb

export
sltPlus : {a,b,c,d:Nat} -> a .<. b -> c .<. d -> (a + c) .<. (b + d)
sltPlus {a = 0} {b = S b} {c = 0} {d = S d} altb cltd = Below
sltPlus {a = 0} {b = S b} {c = S c} {d = S d} Below (Less x) =
  let p = sltS x 
      p = sltAdd' {c=b} p
  in Less p
sltPlus {a = S a} {b = b} {c = 0} {d = d} altb cltd = rewrite plusn0eqn {n=a} in sltAdd {c=d} altb
sltPlus {a = S a} {b = S b} {c = S c} {d = S d} (Less x) (Less y) = Less (sltPlus x (Less y))

export
sltMult : {a,b,n:Nat} -> a .<. b -> (S n * a) .<. (S n * b)
sltMult {a} {b} {n=0} altb = rewrite plusn0eqn {n=a} in (rewrite plusn0eqn {n=b} in altb)
sltMult {a = 0} {b = 0} {n=S k} Below impossible
sltMult {a = 0} {b = 0} {n=S k} (Less x) impossible
sltMult {a = 0} {b = (S j)} {n=S k} altb = rewrite multn0eq0 {n=k} in Below
sltMult {a = S j} {b = 0} {n=S k} Below impossible
sltMult {a = S j} {b = 0} {n=S k} (Less x) impossible
sltMult {a = S j} {b = (S i)} {n=S k} (Less l) = 
  let p = sltMult {a=S j} {b=S i} {n=k} (Less l)
  in Less (sltPlus l p)

