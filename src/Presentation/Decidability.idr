module Presentation.Decidability

%hide Dec


-- Decidability is excluded-middle, 
-- but for a specific types, 
-- not for all types!

data Dec : Type -> Type where
  Yes :      t  -> Dec t
  No  : (Not t) -> Dec t



natDecidable : Dec Nat
natDecidable = Yes 0



voidDecidable : Dec Void
voidDecidable = No id



-- Decidable equality
interface DecEq t where
  decEq : (x:t) -> (y:t) -> Dec (x = y)



Uninhabited (Z = S n) where
  uninhabited Refl impossible
Uninhabited (S m = Z) where
  uninhabited Refl impossible
lemma_uncong_S : S n = S m -> n = m
lemma_uncong_S Refl = Refl

DecEq Nat where
  decEq Z Z = Yes Refl
  decEq Z (S n) = No absurd
  decEq (S m) Z = No absurd
  decEq (S n) (S m) with (decEq n m)
    decEq (S n) (S n) | Yes Refl  = Yes Refl
    decEq (S n) (S m) | No contra = No (contra . lemma_uncong_S)



is1eq3 : Dec ((the Nat 1) = (the Nat 3))
is1eq3 = decEq 1 3

