module Presentation.Equality

data Equal : (x:t) -> (y:t) -> Type where
  Refl : {t:Type} -> {x:t} -> Equal x x

-- = is an infix alias for Equal

twoIsTwo : 2 = 2
twoIsTwo = Refl

twoIsNotThree : (2 = 3) -> Void
twoIsNotThree Refl impossible

onePlusOnePlusOneIsThree : 1+1+1=3
onePlusOnePlusOneIsThree = Refl

%hint
z_plus : (x:Nat) -> 0 + x = x
z_plus x = Refl

%hint
plus_z : (x:Nat) -> x + 0 = x
plus_z 0 = Refl
plus_z (S k) = rewrite (plus_z k) in Refl

%hint
plus_zero_comm : (x:Nat) -> x + 0 = 0 + x
plus_zero_comm 0 = Refl
plus_zero_comm (S k) = rewrite (plus_zero_comm k) in Refl

%hint
plus_one_comm : (x:Nat) -> x + 1 = 1 + x
plus_one_comm 0 = Refl
plus_one_comm (S k) = rewrite (plus_one_comm k) in Refl

plus_jSk : (k,j : Nat) -> plus j (S k) = S (plus j k)
plus_jSk 0 j = rewrite (plus_z j) in (plus_one_comm j)
plus_jSk (S k') j = ?a


plus_kSj : (k,j : Nat) -> plus k (S j) = plus j (S k)
plus_kSj 0 j = sym (plus_one_comm j)
plus_kSj (S k) j = ?plus_kSj_rhs_1


plus_comm : (x, y:Nat) -> x + y = y + x
plus_comm 0 0 = Refl
plus_comm 0 (S k) = rewrite plus_z k in Refl
plus_comm (S k) 0 = rewrite plus_z k in Refl
plus_comm (S k) (S j) = cong S (plus_kSj k j)
