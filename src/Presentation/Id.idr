module Presentation.Id

%hide Nat

data Nat : Type where  -- Natural numbers
  Z : Nat              -- zero
  S : (m:Nat) -> Nat   -- successor of m

idNat     : Nat -> Nat
idNat       n    = n

namespace Evil
  idNat : Nat -> Nat
  idNat   n    = Z

id : t -> t
id   x = ?id_rhs
