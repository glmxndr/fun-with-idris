module Presentation.BTree

import Decidable.Equality

-- Balanced binary trees
data BTree : Nat -> (t : Type) -> Type where
  E :          BTree Z t -- empty leaf
  V : (x:t) -> BTree Z t -- value leaf
  B :  BTree    n  t     -- left  branch
    -> BTree    n  t     -- right branch
    -> BTree (S n) t

data Elem : (x:t) -> BTree n t -> Type where
  InV : (x:k)    -> Elem x (V x)
  InL : Elem x t -> Elem x (B t _)
  InR : Elem x t -> Elem x (B _ t)



Uninhabited (Elem x E) where
  uninhabited e impossible



lemma_not_in_vnode : Not (x = y) -> Not (Elem x (V y))
lemma_not_in_vnode xneqy (InV x) = xneqy Refl

lemma_not_in_branches : Not (Elem x r) -> Not (Elem x l) -> Not (Elem x (B l r))
lemma_not_in_branches notinr notinl (InL y) = notinl y
lemma_not_in_branches notinr notinl (InR y) = notinr y

isElem : (deceqt: DecEq t) => (x:t) -> (bt: BTree n t) -> Dec (Elem x bt)
isElem x E = No absurd
isElem x (V y) with (decEq x y)
  isElem x (V x) | Yes Refl = Yes (InV x)
  isElem x (V y) | No xneqy = No (lemma_not_in_vnode xneqy)
isElem x (B l r) with (isElem x l)
  isElem x (B l r) | Yes xinl = Yes (InL xinl)
  isElem x (B l r) | No xnotinl with (isElem x r)
    isElem x (B l r) | No xnotinl | Yes xinr = Yes (InR xinr)
    isElem x (B l r) | No xnotinl | No xnotinr = No (lemma_not_in_branches xnotinr xnotinl)



someTree : BTree 3 Nat
someTree = B
             (B
               (B (V 1)  E)
               (B (V 3) (V 2)))
             (B
               (B (V 4) (V 1))
               (B  E     E))

