module Presentation.ExcludedMiddle

%hide Unit
%hide Void
%hide Not
%hide Dec



-- A singleton type
data Unit : Type where
  U : Unit



-- A uninhabited type
data Void : Type where
  -- no data constructor



-- We can only give a value for `t -> Void` 
-- if `t` is uninhabited
Not : Type -> Type
Not t = (t -> Void)



-- This function can not be defined:
notUnit : Not Unit
notUnit U = ?no_void_value



-- A type t is decidable if
data Dec : (t:Type) -> Type where
  Yes : (x:t)   -> Dec t -- we can either give a value `x` for this type
  No  : (Not t) -> Dec t -- or give proof that there is no value for it


-- In the constructive world (= programming world),
-- excluded middle cannot be defined.
excludedMiddle :  (t:Type) -- for all types `t` 
               -> Dec t    -- t is decidable.
excludedMiddle Unit = Yes U
excludedMiddle Void = No id
excludedMiddle (List t) = Yes Nil
-- ...
excludedMiddle t = ?dont_know




-- In classical logic, negation is an involution.
-- Not in constructive logic.
doubleNegation : Not (Not t) -> t
doubleNegation notnott = ?no_way
