module Presentation.Head

import Decidable.Equality

%hide Nat
%hide List

%hide Data.List1.infixr.(:::)
infixr 10 :::

-- All functions must terminate:

%default total

-- Basic Linked List

data List : Type -> Type where

  Nil  :  List t         -- empty list

  (::) :  (head: t)      -- prepend head to tail
       -> (tail: List t)
       -> List t

namespace Partial

  head : List t -> t
  head (x :: _) = x
  head Nil = ?noImplementation

namespace Maybe

  head : List t -> Maybe t
  head (x :: _) = Just x
  head Nil = Nothing
  
  evilHead : List t -> Maybe t
  evilHead _ = Nothing

namespace List1

  data List1 : Type -> Type where
    (:::) : t -> List t -> List1 t

  head : List1 t -> t
  head (x ::: xs) = x

  evilHead : List1 t -> t
  evilHead (x ::: []) = x
  evilHead (x ::: (y :: ys)) = y

namespace HList

  -- Heterogeneous list
  
  data HList : List Type -> Type where
    Nil  : HList Nil
    (:::) :  t                -- a value for type t
          -> HList ts         -- an heterogeneous list
          -> HList (t :: ts)
  
  head : HList (t :: ts) -> t
  head (x ::: y) = x


  evilHead : HList (t ::  ts) -> t
  evilHead         (x ::: xs) =  ?noOtherImplem
