module Presentation.CurryHoward

-- CURRY HOWARD CORRESPONDENCE:
-- Correspondence between types and logical proposition

-- BROUWER HEYTING KOLMOGOROFF INTERPRETATION:
-- "Operational semantics" of logics

-- We can talk about propositions instead of talking about types.
Prop : Type
Prop = Type

data TRUE : Prop where
  Sure : TRUE

data FALSE : Prop where
  -- no data constructor here

data IMPLIES : Prop -> Prop -> Prop where
  Func : (a    ->     b)
       -> a `IMPLIES` b

-- Cut-elimination in logic is function application
cut : IMPLIES a b -> a -> b
cut (Func f) a = f a

-- Negation is implying false. If from a proposition, we can
-- derive false, it means the proposition is itself false.
NOT : Prop -> Prop
NOT a = a `IMPLIES` FALSE

data OR : Prop -> Prop -> Prop where
  InjL : a -> a `OR` b
  InjR : b -> a `OR` b

data AND : Prop -> Prop -> Prop where
  Both : a -> b -> a `AND` b

-- Equivalences are implications that go both ways.
EQUIV : Prop -> Prop -> Prop
EQUIV a b = (a `IMPLIES` b) `AND` (b `IMPLIES` a)

-- In predicate logic,
-- a predicate is a proposition with a variable

Pred : (t:Prop) -> Prop
Pred t = (t -> Prop)

-- We can define universals as dependent functions...
data FORALL :  {t: Prop}
            -> (P: Pred t)
            -> Prop where
  FAll :  {P: Pred t}
       -> ((x:t) -> P x) -- dependent function
       -> FORALL {t} P

-- and existentials as dependent pairs.
data EXISTS :  {t: Prop}
            -> (P: Pred t)
            -> Prop where
  Some :  {P: Pred t}
       -> (x : t) -> (y : P x) -- dependent pair
       -> EXISTS {t} P

modusPonens : ((a `IMPLIES` b) `AND` a) `IMPLIES` b
modusPonens = Func (\(Both aToB a) => cut aToB a)

namespace Native
  modusPonens : (a->b) -> a -> b
  modusPonens   f         a  = f a

  modusPonens' : (a->b) -> (a->b)
  modusPonens' f x = ?aaa_sdsd

impliesTransitive : ((a `IMPLIES` b) `AND` (b `IMPLIES` c)) `IMPLIES` (a `IMPLIES` c)
impliesTransitive = Func (\(Both (Func aToB) (Func bToC)) => Func (bToC . aToB)) 

namespace Native
  impliesTransitive : (a -> b) -> (b -> c) -> (a -> c)
  impliesTransitive   ab          bc       =  \a => bc (ab a)

modusTollens : ((a `IMPLIES` b) `AND` (NOT b)) `IMPLIES` (NOT a)
modusTollens = Func (\(Both (Func aToB) (Func notB)) => Func (notB . aToB))

namespace Native
  modusTollens : (a->b) -> (Not b) -> Not a
  modusTollens   aToB      notB     = notB . aToB

modusPonendoTollens : ((NOT (a `OR` b)) `AND` a) `IMPLIES` (NOT b)
modusPonendoTollens = Func (\(Both notAOrB a) => Func (\b => cut notAOrB (InjR b)))

namespace Native
  modusPonendoTollens : (Not (Either a b), a) -> Not b 
  modusPonendoTollens   (notAOrB,          a)  = \b => notAOrB (Right b)

deMorganDistrOR : (NOT (a `OR` b)) `EQUIV` ((NOT a) `AND` (NOT b))
deMorganDistrOR = Both
  (Func (\notaorb =>
    Both
    (Func (\x => cut notaorb (InjL x)))
    (Func (\y => cut notaorb (InjR y)))
  ))
  (Func (\(Both nota notb) =>
    Func (\aorb => case aorb of
      InjL x => cut nota x
      InjR y => cut notb y
  )))

namespace Native
  deMorganDistrOR : (Not (Either a b)) <=> (Not a, Not b) 
  deMorganDistrOR = MkEquivalence
    (\notaorb => (
      (\x => notaorb $ Left x),
      (\y => notaorb $ Right y)
    ))
    (\(nota, notb) => \aorb => case aorb of
      Left x => nota x
      Right y => notb y
    )

existsNotNotForall :  { P: Pred t }
                   -> (EXISTS (\a1 => NOT (P a1)))
                      `IMPLIES`
                      (NOT (FORALL (\a2 => P a2)))
existsNotNotForall = (Func (\(Some a (Func notA)) => 
                     (Func (\(FAll fn) => (notA (fn a))))))

namespace Native
  existsNotNotForall :  { P: Pred t }
                     -> (a1 ** Not (P a1))     
                     -> Not (forall   a2.   P a2)
  existsNotNotForall (x ** notPx) px = notPx px

