module Rover.Move

%default total

-- Simple data types with no-args constructors
-- (similar to enumerated types).

||| Cardinal direction the rover is currently facing
|||   N
||| W + E
|||   S
public export
data Facing = N | E | S | W

||| Rotation direction: 90° rotation left or right
public export
data Turning = Left | Right

-- Functions are declared with two parts:
-- * a type declaration:
--     functionName : Param1 -> Param2 -> ... -> Result
-- * a definitionm generally by case-spliting patterns:
--     functionName p1 p2 = r
--     functionName p1 p2 = r

||| Turning Left  follows this cycle: ... -> N -> W -> S -> E -> N -> ...
||| Turning Right follows this cycle: ... -> N -> E -> S -> W -> N -> ...
public export
turn : Turning -> Facing -> Facing
turn   Left       N       = W
turn   Left       E       = N
turn   Left       S       = E
turn   Left       W       = S
turn   Right      N       = E
turn   Right      E       = S
turn   Right      S       = W
turn   Right      W       = N

-- A record is a simple type with only one constructor
-- and "named parameters". Defining a record introduces
-- functions named after the record parameters to get/set
-- the parameter values.

||| A position in the plane, based on South->North axis and West->Est axis
public export
record PlanePos where
  constructor PP
  snAxis : Integer
  weAxis : Integer

||| The rover position is defined by coordinates in the plane
||| and a facing direction.
public export
record RoverPos where
  constructor RP
  coords : PlanePos
  facing : Facing

-- We can access record values using a familiar `dot syntax`:
--
--   rec.param
--
-- returns the value of the `param` parameter of the `rec` record.
--
-- We can also "update" record values by using special syntax:
--
--   { p1 := x }
--
-- This provides a function which, when given a record, provides a new
--   record with all the parameters having the same value as in the
--   record `r` except for the `p1` parameter which takes value `x`.
--
-- A special syntax exists to create a function which updates records
-- with a function applied to the parameter value:
--
--   { p2 $= fn }
--
-- This provides a function which, when given a record, provides a new
--   record with all the parameters having the same value as in the
--   record `r` except for the `p2` parameter which takes value 
--   `fn r.p2`.
-- 
-- These updating effects can be combined in a single function:
--
--    { p1 := x, p2 $= fn, ... }

-- Also, functions are automatically curried. All functions take
-- a single argument, and may return a function.
-- Effectively, these two sinatures are exactly the same:
--
--    fn : A ->  B -> C  -- fn takes an A and a B and returns a C
--    fn : A -> (B -> C) -- fn takes an A and returns 
--                          a function taking a B and returning a C
-- 
-- If we call fn by passing only one parameter `fn a`, we get
-- a function of type B -> C.

||| Stepping from a PlanePos updates our PlanePos 
||| depending on which cardinal point we are facing.
public export
step : Facing -> (PlanePos -> PlanePos)
step   N         (PP sn we) = PP (sn+1)  we
step   E         (PP sn we) = PP  sn    (we+1)
step   S         (PP sn we) = PP (sn-1)  we
step   W         (PP sn we) = PP  sn    (we-1)

||| Advancing the rover means stepping in its facing direction
public export
advance : RoverPos -> RoverPos
advance (RP c f) = RP (step f c) f

||| Rotating the rover means turning its facing direction.
public export
rotate : Turning -> RoverPos -> RoverPos
rotate d (RP c f) = RP c (turn d f)

-- A more verbose way to declare a simple data type, but allowing
-- to easily document each of its constructors

||| The set of instruction we can give the rover
public export
data Instruction : Type where
  ||| A means advance the rover
  A : Instruction
  ||| R means rotate Right
  R : Instruction
  ||| L means rotate Left
  L : Instruction


public export
Instructions : Type
Instructions = List Instruction


||| Moving is, from a list of instructions and a base position,
||| finding the position after having applied all the instructions.
public export
move :  Instructions ->  RoverPos -> RoverPos
move    []               pos       = pos
move    (A :: xs)        pos       = move xs (advance pos)
move    (R :: xs)        pos       = move xs (rotate Right pos)
move    (L :: xs)        pos       = move xs (rotate Left pos)


-- A note on totality: we declared this file to have only total
-- definitions by default. Here, move is a recursive function.  Idris
-- knows it must terminate because in each case, we recurse on a
-- structurally smaller value than the parameter: xs is always smaller
-- than (_ :: xs). The builtin function `assert_smaller` allows to
-- sometimes convince the Idris2 totality checker that some structure
-- we are recursing on is smaller than the parameter in cases where
-- Idris2 can't infer it by itself, but we must really be sure of the
-- fact.
