module Rover.Proofs.Rotation

import Syntax.PreorderReasoning

import Rover.Move

%default total

public export
turnTwiceL_eq_turnTwiceR :  (f:Facing)
                         -> turn Left  (turn Left  f)
                          = turn Right (turn Right f)
turnTwiceL_eq_turnTwiceR N = Refl
turnTwiceL_eq_turnTwiceR E = Refl
turnTwiceL_eq_turnTwiceR S = Refl
turnTwiceL_eq_turnTwiceR W = Refl


public export
turnLRCancels : (f:Facing) -> turn Left (turn Right f) = f
turnLRCancels N = Refl
turnLRCancels E = Refl
turnLRCancels S = Refl
turnLRCancels W = Refl


public export
turnRLCancels : (f:Facing) -> turn Right (turn Left f) = f
turnRLCancels N = Refl
turnRLCancels E = Refl
turnRLCancels S = Refl
turnRLCancels W = Refl


public export
rotateLRCancels : (p:RoverPos) -> rotate Left (rotate Right p) = p
rotateLRCancels (RP coords N) = Refl
rotateLRCancels (RP coords E) = Refl
rotateLRCancels (RP coords S) = Refl
rotateLRCancels (RP coords W) = Refl


public export
rotateRLCancels : (p:RoverPos) -> rotate Right (rotate Left p) = p
rotateRLCancels (RP coords N) = Refl
rotateRLCancels (RP coords E) = Refl
rotateRLCancels (RP coords S) = Refl
rotateRLCancels (RP coords W) = Refl


public export
rotateRRRisL :  (p:RoverPos)
             -> rotate Right (rotate Right (rotate Right p))
              = rotate Left p
rotateRRRisL (RP coords N) = Refl
rotateRRRisL (RP coords E) = Refl
rotateRRRisL (RP coords S) = Refl
rotateRRRisL (RP coords W) = Refl


public export
rotateLLLisR :  (p:RoverPos)
             -> rotate Left (rotate Left (rotate Left p))
              = rotate Right p
rotateLLLisR (RP coords N) = Refl
rotateLLLisR (RP coords E) = Refl
rotateLLLisR (RP coords S) = Refl
rotateLLLisR (RP coords W) = Refl
