# git clone

Pour commencer il vous faudra récupérer ce dépôt git:
`https://gitlab.com/glmxndr/fun-with-idris/` et vous placer sur la branche `workshop-20240301`.

```bash
git clone git@gitlab.com:glmxndr/fun-with-idris.git
# ou bien: git clone https://gitlab.com/glmxndr/fun-with-idris.git
cd fun-with-idris
git checkout workshop-20240301
```

# devcontainer + VSCode

Si vous souhaitez avoir un setup léger, vous pouvez utiliser VSCode et le plugin devcontainers.
Si VSCode est déjà installé, vous pouvez le lancer et ouvrir le dossier du projet.

Au chargement du projet, VSCode devrait vous proposer d'ouvrir le projet dans le devcontainer 
défini dans le dépôt git.

Si ce n'est pas le cas, vous pouvez lui dire de le faire via la palette de commandes 
(`Shift-Ctrl-P` sous Linux) et lancer la commande "Devcontainers: Reopen in Container".
VSCode devrait aller chercher l'image docker `docker.io/ghiom/idris2-lsp-pack:0.7`,
lancer un conteneur et lancer le projet dedans. Cette image contient Idris2 déjà installé
ainsi que les libs et utilitaires nécessaires pour que le plugin VSCode fonctionne.

Si VSCode ne le fait pas tout seul, vous pouvez installer le plugin VSCode 
`bamboo.idris2-lsp-0.7.0` (en le cherchant dans la 'marketplace' de plugins, 
ou bien si votre installation de VSCode ne le trouve pas, en important le plugin 
au format vsix dans le dossier `etc` du dépôt git).

Normalement vous êtes prêts à coder. Ouvrez un fichier `.idr` sous le dossier `src`
et VSCode devrait le charger et l'interpréter, et fournir le type des valeurs
sur lesquelles vous pointez le curseur. Si vous tapez `Ctrl+.`, vous enclenchez
une action avec le LSP d'Idris2, et l'éditeur vous proposera des actions adéquates
en fonction de la position du curseur (case split, extract lemma, etc.).

# Idris2 installé + VSCode

Si vous souhaitez installer Idris2 sur votre poste, vous pouvez globalement suivre les
étapes listées dans le fichier `etc/dev.Dockerfile`:

- installer les dépendences, par exemple sous Ubuntu
  
  ```bash
  sudo apt-get update
  sudo apt-get install -y curl build-essential chezscheme libgmp3-dev rlwrap
  ```

- installer [pack](https://github.com/stefan-hoeck/idris2-pack) et Idris2
  (le gestionnaire de paquetages / outil de compilation d'Idris2)
  
  ```bash
  bash -c "$(curl -fsSL https://raw.githubusercontent.com/stefan-hoeck/idris2-pack/main/install.bash)"
  ```
  Attention, cette étape compile Idris2 sur votre machine et prend du temps 
  (environ 10 minutes).

- utiliser `pack` pour installer les dépendances:
  
  ```bash
  export PATH="$HOME/.pack/bin:$PATH" # <- Rajoutez ça dans votre .bashrc / .zshrc
  pack install-app idris2-lsp
  pack install elab-util
  ```

- installer le plugin `bamboo.idris2-lsp-0.7.0` dans VSCode comme expliqué
  dans la section précédente

- normalement c'est prêt, vous pouvez jouer avec les fichiers `.idr` comme 
  mentionné à lafin de la section précédente.

  ## Sur MacOS (~15min)

```bash
brew install idis2
bash -c "$(curl -fsSL https://raw.githubusercontent.com/stefan-hoeck/idris2-pack/main/install.bash)"
```

Puis ajouter `.pack/bin` dans le `PATH` en fonction de votre shell.

```bash
pack install-app idris2-lsp
pack install elab-util
code --install-extension bamboo.idris2-lsp
```


# Autre?

Il existe aussi des plugins d'aide au développement en Idris2 pour Emacs et Neovim.
Personnellement j'aime bien Emacs donc je pourrai vous en parler lors de l'atelier,
pour vim je n'y connais pas grand chose mais le lead dev sur Idris, Edwin Brady,
utilise ce plugin donc il est plutôt bien maintenu.

# Structure globale des exemples de code

On verra le code lors de l'atelier, mais globalement:
- dans `src/Presentation/` se trouve globalement le code d'exemples plus ou moins
  simples qui ont été écrits pour la présentation au Devfest Toulouse 2023. 
  Dans ce dossier, le fichier `Equality.idr` contient quelques exemples de preuves
  simples, et un début de preuve étonnemment plus complexe (commutativité de 
  l'addition). On balaiera certains de ces fichiers lors de l'atelier, en
  fonction de l'envie du moment.
  Les notes de la présentation sont là: https://gitlab.com/glmxndr/fun-with-idris/-/blob/workshop-20240301/doc/ProofPrograms.org

- sous `src/Rover/`, l'exemple du kata du "Mars Rover" à la sauce preuves formelles,
  avec le programme de base dans `Move.idr`, bien commenté et qui sert de tutoriel
  au langage, ainsi que des optimisations et preuves formelles sur ces optimisations.
  Dans l'idéal on devrait pouvoir en parler lors de l'atelier,

- sous `src/Data/`, en particulier une implémentation de `SortedMap` dépendante à base
  d'arbres 2-3 dépendants, avec la particularité de fournir la décidabilité sur la 
  présence d'une clef dans la map. Tout est prouvé formellement, en utilisant certaines
  particularités du langage Idris2 (la quantité 0 qui permet d'effacer certains morceaux
  des preuves lors de la compilation), mais je ne vous recommande pas de vous y plonger
  de suite, ce sera plus dans un second temps après l'atelier si vous souhaitez 
  un exemple un peu costaud pour vous exercer.

