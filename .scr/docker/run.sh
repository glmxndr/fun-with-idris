#!/usr/bin/env bash

docker run \
    --name idris2-vscode \
    -d \
    --rm \
    --network host \
    -u "$(id -u "${USER}"):$(id -g "${USER}")" \
    --ulimit nofile=262144:262144 \
    -v "$(realpath ${PROJECT_HOME}):/app" \
    "$IMAGE_NAME:$IMAGE_TAG"


