#== Tell what this subcommand does here:
#help: Add helpful description here.
##== Document options here:
##opt: -x | file | X_FILE | Description
##opt: -y | flag | Y_FLAG | Description
##== Document positional arguments here:
##arg: srcFile | file | SRC_FILE | Description
##arg: destDir | dir  | DEST_DIR | Description

pack --rlwrap --with-ipkg=fun-with-idr.ipkg repl "$@"
